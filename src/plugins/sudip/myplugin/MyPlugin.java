package plugins.sudip.myplugin;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.frame.progress.ToolTipFrame;
import icy.gui.main.GlobalROIListener;
import icy.gui.plugin.PluginRichToolTip;
import icy.image.IcyBufferedImage;
import icy.plugin.PluginDescriptor;
import icy.plugin.abstract_.PluginActionable;
import icy.roi.ROI;
import icy.roi.ROIEvent;
import icy.roi.ROIListener;
import icy.roi.ROIEvent.ROIEventType;
import icy.sequence.Sequence;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceListener;
import icy.roi.*;

import icy.roi.ROI2D;

import java.awt.Event;
import java.util.*;

public class MyPlugin extends PluginActionable  implements ROIListener, SequenceListener {
	
	
	int alternator = 1;
	@Override
	public void run() {
		
		Sequence sequence = getActiveSequence();
		
		List<ROI> roi = sequence.getROIs();
		
		
		
		
//		torename.addListener( new ROIListener(){
//			
//			public void roiChanged(ROIEvent event)
//			{
//				testing obj1 = new testing(torename);
//				obj1.runtesting();
//			}
//		});
		
		
		Iterator<ROI> itr = roi.iterator();
		
		
		
		while (itr.hasNext())
		{
			ROI temp=(ROI)itr.next();
			new AnnounceFrame(temp.toString());
			
			temp.addListener (this);
	
	}

	
}

	@Override
	public void roiChanged(ROIEvent eventt) {
		// TODO Auto-generated method stub
		ROI changedroi = eventt.getSource();
		
		
	    ROIEvent.ROIEventType event =  eventt.getType();
	    
		
	    
	    new AnnounceFrame("calling for the event type:=>"+event);
	      if (event==ROIEvent.ROIEventType.NAME_CHANGED || event == ROIEvent.ROIEventType.PROPERTY_CHANGED  ) {new AnnounceFrame("inside ROIEvent.ROIEventType");}
	      else
		 {
		testing obj = new testing(changedroi);
		if ((alternator++)%2==1) obj.runtesting();
		}
		
	}

	@Override
	public void sequenceChanged(SequenceEvent sequenceEvent) {
		// TODO Auto-generated method stub
		
//		new AnnounceFrame("sequence event triggered");
//		
//		Sequence seq = sequenceEvent.getSequence();
//		
//		new AnnounceFrame("Loaded images in sequence=="+seq.getNumImage());
//		//if (seq!=null) new AnnounceFrame("sequence is not null");
//		
//		ROI torename = seq.getFocusedROI();
//		
//		if (torename==null) new AnnounceFrame("Still roi is null ");
//		
//		testing obj1 = new testing(torename);
//		
//		
//		obj1.runtesting();
		
	}

	@Override
	public void sequenceClosed(Sequence sequence) {
		// TODO Auto-generated method stub
		
	}

	
}
